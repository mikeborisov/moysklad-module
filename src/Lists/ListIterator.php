<?php

namespace MoySklad\Lists;

/**
 * Iterator for EntityList
 * Class ListIterator
 * @package MoySklad\Lists
 */
final class ListIterator implements \Iterator{
    private
        $items = [],
        $cursor = 0;

    public function __construct(&$items)
    {
        $this->items = $items;
    }

    public function rewind(): void
    {
        $this->cursor = 0;
    }

    public function valid(): bool
    {
        return array_key_exists($this->cursor, $this->items);
    }

    public function key(): mixed
    {
        return $this->cursor;
    }

    public function current(): mixed
    {
        return $this->items[$this->cursor];
    }

    public function next(): void
    {
        ++$this->cursor;
    }
}



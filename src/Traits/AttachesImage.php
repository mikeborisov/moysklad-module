<?php

namespace MoySklad\Traits;

use MoySklad\Components\Fields\ImageField;
use MoySklad\Entities\AbstractEntity;

trait AttachesImage{
    public function attachImage(ImageField $imageField){
        if(!isset($this->fields->images)) {
            $this->fields->images = [];
        }
        $imagesTmp = $this->fields->images;
        $imagesTmp[] = $imageField;
        /**
         * @var AbstractEntity $this
         */
        $this->fields->images = $imagesTmp;
    }
}
